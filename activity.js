fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
		let records = data;
		const result = records.map(x => x.title)	
		console.log([result])

	})

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => {
	let records = data;	
		console.log(records)
});

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => {
	let records = data;	
		console.log( 'The item "'+ records.title + '" on the list has a status of ' + records.completed)
});

fetch('https://jsonplaceholder.typicode.com/todos',{
method: 'POST',
headers: {
	'Content-type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		id: 201,
		title: 'Created to do list item',
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data));

fetch('https://jsonplaceholder.typicode.com/todos/1',{
method: 'PUT',
headers: {
	'Content-type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update my to do list with a different data structure",
		id: 1,
		status: "Pending",
		title: 'Update to do list item',
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data));


fetch('https://jsonplaceholder.typicode.com/todos/1',{
method: 'PUT',
headers: {
	'Content-type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		id: 1,
		status: "Complete",
		title: 'delectus aut autem',
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data));

	